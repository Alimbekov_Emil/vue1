import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

import Home from "@/Pages/Home";
import NotFound from "@/Pages/404";
import Author from "@/Pages/Author";
import AddPhoto from "@/Pages/AddPhoto";
import Register from "@/Pages/Register";
import Login from "@/Pages/Login";
import { loadFromLocalStorage } from "./helper/localStorage";

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/author/:id",
      name: "author",
      component: Author,
    },
    {
      path: "/add",
      name: "addPhoto",
      component: AddPhoto,
      beforeEnter: (to, from, next) => {
        const user = loadFromLocalStorage();
        if ((to.name === "addPhoto") & user) next("/add");
        else next();
      },
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
    },

    {
      path: "*",
      name: "NotFound",
      component: NotFound,
    },
  ],
});
