import axiosApi from "../axios";
import { loadFromLocalStorage } from "./localStorage";

export default function setup() {
  axiosApi.interceptors.request.use((config) => {
    const token = loadFromLocalStorage();
    try {
      config.headers["Authorization"] = token.token;
    } catch (e) {
      // console.log(e);
    }
    return config;
  });

  axiosApi.interceptors.response.use(
    (res) => res,
    (e) => {
      if (!e.response) {
        e.response = { data: { global: "No internet" } };
      }
      throw e;
    }
  );
}
