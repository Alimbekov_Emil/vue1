import axiosApi from "../../axios";
import routes from "../../routes";

export default {
  actions: {
    async postPhotoAction({ commit }, photo) {
      try {
        await axiosApi.post("photos", photo);
        routes.push("/");
      } catch (e) {
        commit("postPhotoMutations", e);
      }
    },
  },
  mutations: {
    postPhotoMutations(state, error) {
      state.error = error;
    },
  },
  state: {
    error: null,
  },
};
