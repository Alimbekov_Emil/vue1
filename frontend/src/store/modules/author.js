export default {
  actions: {
    async fetchAuthorGalleryAction({ commit }, id) {
      const response = await fetch("http://localhost:8001/photos?user=" + id).then((response) =>
        response.json()
      );

      commit("fetchAuthorGalleryMutations", response);
    },
  },
  mutations: {
    fetchAuthorGalleryMutations(state, gallery) {
      state.galleryAuthor = gallery;
    },
  },
  state: {
    galleryAuthor: [],
  },
  getters: {
    fetchAuthorGalleryGetters(state) {
      return state.galleryAuthor;
    },
  },
};
