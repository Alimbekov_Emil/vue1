export default {
  actions: {
    async fetchGalleryAction({ commit }) {
      const response = await fetch("http://localhost:8001/photos").then((response) => response.json());
      commit("fetchGalleryMutations", response);
    },
  },

  mutations: {
    fetchGalleryMutations(state, gallery) {
      state.gallery = gallery;
    },
  },
  state: {
    gallery: [],
  },
  getters: {
    fetchGalleryGetters(state) {
      return state.gallery;
    },
  },
};
