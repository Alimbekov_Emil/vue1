import axiosApi from "../../axios";
import { loadFromLocalStorage, saveToLocalStorage } from "../../helper/localStorage";
import routes from "../../routes";

export default {
  actions: {
    async registerUserAction({ commit }, user) {
      try {
        const response = await axiosApi.post("/users", user);
        commit("registerSuccessUserMutations", response.data);
        routes.push("/");
      } catch (e) {
        commit("registerErrorUserMutations", e);
      }
    },

    async loginUserAction({ commit }, user) {
      try {
        const response = await axiosApi.post("/users/sessions", user);
        commit("loginSuccessUserMutations", response.data.user);
        routes.push("/");
      } catch (e) {
        commit("loginErrorUserMutations", e);
      }
    },

    async logoutUserAction({ commit }) {
      try {
        await axiosApi.delete("/users/sessions");
        commit("logoutSuccessMutations");
      } catch (e) {
        commit("logoutErrorMutations", e);
      }
    },

    saveUserAction({ commit }) {
      if (loadFromLocalStorage()) {
        commit("saveUserMutations", loadFromLocalStorage());
      } else {
        routes.push("/login");
      }
    },
  },
  mutations: {
    registerErrorUserMutations(state, error) {
      state.error = error;
    },
    registerSuccessUserMutations(state, user) {
      state.user = user;
      saveToLocalStorage(state.user);
    },
    loginErrorUserMutations(state, error) {
      state.error = error;
    },
    loginSuccessUserMutations(state, user) {
      state.user = user;
      saveToLocalStorage(state.user);
    },

    logoutSuccessMutations(state) {
      state.user = null;
      saveToLocalStorage(null);
    },

    logoutErrorMutations(state, error) {
      state.error = error;
    },

    saveUserMutations(state, user) {
      state.user = user;
    },
  },
  state: {
    error: null,
    user: false,
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
};
