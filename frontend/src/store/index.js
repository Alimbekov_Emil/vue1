import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import home from "./modules/home";
import author from "./modules/author";
import addPhoto from "./modules/addPhoto";
import users from "./modules/users";

export default new Vuex.Store({
  modules: { home, author, addPhoto, users },
});
