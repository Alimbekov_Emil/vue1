import Vue from "vue";
import App from "./App.vue";

import router from "./routes";
import store from "./store";

import setup from "./helper/interseptors";

setup();

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
