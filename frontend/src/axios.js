import axios from "axios";
import { BASE_URL } from "./helper/config";

const axiosApi = axios.create({
  baseURL: BASE_URL,
});

export default axiosApi;
